$( document ).ready(function() {

	//show download panels ----------------------------------------------------------------
	var found = 0;
	var missing = 0;

	//get current page url and parse. to get ulSelector ----------------------------------------------------------------

	$("#nav").load('includes/nav.html', function() {
	 	
	 	var ulSelector = '#ul-' + $('meta[name=pageid]').attr('content');
		//console.log(ulSelector);

		//nav collapse/expand
		if (ulSelector == '#ul-index') {
			$("#ul-lockups").show();
		}
		else {
			$(ulSelector).show();
		}
		$("#nav h3").click(function(){
			//$(this).next('ul').slideToggle();
			window.location = $(this).attr('href');
		});

		//smooth scrolling ----------------------------------------------------------------
		$.localScroll({
			hash: true,
			duration: 500,
			offset: -100
		});

	});

	$("#footer").load('includes/footer.html', function() {
	});

	$('a.dlink').each(function() {
		var did = 		$(this).parent().parent().parent().attr('dstring').toUpperCase();
		var dalign = 	$(this).parent().parent().attr('dstring').toUpperCase();
		var dbg = 		$(this).parent().attr('dstring').toUpperCase();
		
		var dlang = 	$(this).attr('lang');
		var dtext = 	$(this).parent().parent().parent().parent().parent().siblings('h2').attr('dstring');
		var dtype = 	$(this).attr('dstring');
		var dformat = 	$(this).attr('format');
		var ddir = 		dtype.toUpperCase();


		//lang in EN by default
		if (dlang == "" || typeof dlang == "undefined"){
			dlang = "EN";
		}
		dlang = dlang.toUpperCase();

		if (typeof dformat != "undefined" && dtype !== dformat) {
			dtype = 	dtype + '.' + dformat;
		}

		var dpath = '_downloads/1-basic/' + ddir + '/' + did + '-' + dalign + '-' + dbg + '-' + dlang + '-' + dtext + '.' + dtype;

		$(this).attr('href', dpath);

		//test link
		// UrlExists(dpath, function(status){
		//     if(status === 200){
		//        // file was found
		//        found++;
		//        console.log('found: ' + dpath + '('+ found+')');
		//     }
		//     else if(status === 404){
		//        // 404 not found
		//        missing++;
		//        console.log('NOT found: ' + dpath + '('+ missing+')');
		//     }
		// });

	});

	function UrlExists(url, cb){
	    jQuery.ajax({
	        url:      url,
	        dataType: 'text',
	        type:     'GET',
	        complete:  function(xhr){
	            if(typeof cb === 'function')
	               cb.apply(this, [xhr.status]);
	        }
	    });
	}

	

	//nav stops scrolling
	$(window).scroll(function() {
		var scrolltop = $(window).scrollTop();
		if (scrolltop > 100){
			$("#nav").css({
				position: 'fixed',
				top: '60px'
			});
		}
		else {
			$("#nav").css({
				position: 'relative',
				top: '0px'
			});
		}
	});

	//dropdown ----------------------------------------------------------------
	$( ".download_button" ).click(function() {
		$('.dropdown').hide();
		$('.dropdown2').hide();
		$(' .dropdown',this).show();
		$(' .dropdown2',this).show();
		$('body').append("<div class='click-away'>&nbsp;</div>");
	});

	//click away
	$('body').on('click', '.click-away', function() {
        $('.click-away').remove();
        $('.dropdown').hide();
        $('.dropdown2').hide();
    });

});